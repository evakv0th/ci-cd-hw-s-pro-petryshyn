import dotenv from 'dotenv';
import express, { Express, Request, Response } from 'express';

dotenv.config();

const app: Express = express();
const port = process.env.PORT || 8081;

app.get('/', (req: Request, res: Response) => {
  try {
    res.send(
      `Express + Typescript Server. Timestamp: ${new Date().toISOString()}`,
    );
  } catch (error) {
    console.error('Error handling request:', error);
    res.status(500).send('Test Error');
  }
});

app.listen(port, () => {
  console.log(`server is running on http://localhost:${port}`);
});
